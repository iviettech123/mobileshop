/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.PaymentEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface PaymentEntityFacadeLocal {

    void create(PaymentEntity paymentEntity);

    void edit(PaymentEntity paymentEntity);

    void remove(PaymentEntity paymentEntity);

    PaymentEntity find(Object id);

    List<PaymentEntity> findAll();

    List<PaymentEntity> findRange(int[] range);

    int count();
    
}
