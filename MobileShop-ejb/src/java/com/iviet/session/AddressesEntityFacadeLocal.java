/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.AddressesEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface AddressesEntityFacadeLocal {

    void create(AddressesEntity addressesEntity);

    void edit(AddressesEntity addressesEntity);

    void remove(AddressesEntity addressesEntity);

    AddressesEntity find(Object id);

    List<AddressesEntity> findAll();

    List<AddressesEntity> findRange(int[] range);

    int count();
    
}
