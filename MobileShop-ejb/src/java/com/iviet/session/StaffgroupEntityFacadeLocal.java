/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.StaffgroupEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface StaffgroupEntityFacadeLocal {

    void create(StaffgroupEntity staffgroupEntity);

    void edit(StaffgroupEntity staffgroupEntity);

    void remove(StaffgroupEntity staffgroupEntity);

    //Khong can viet mo, ma dung ham find nay.
    StaffgroupEntity find(Object id);

    List<StaffgroupEntity> findAll();

    List<StaffgroupEntity> findRange(int[] range);

    int count();
    
}
