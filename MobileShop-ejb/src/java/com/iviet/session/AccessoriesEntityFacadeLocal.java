/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.AccessoriesEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface AccessoriesEntityFacadeLocal {

    void create(AccessoriesEntity accessoriesEntity);

    void edit(AccessoriesEntity accessoriesEntity);

    void remove(AccessoriesEntity accessoriesEntity);

    AccessoriesEntity find(Object id);

    List<AccessoriesEntity> findAll();

    List<AccessoriesEntity> findRange(int[] range);

    int count();
    
}
