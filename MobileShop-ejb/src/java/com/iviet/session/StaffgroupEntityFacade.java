/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.StaffgroupEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author XuânBá
 */
@Stateless
public class StaffgroupEntityFacade extends AbstractFacade<StaffgroupEntity> implements StaffgroupEntityFacadeLocal {
    @PersistenceContext(unitName = "MobileShop-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StaffgroupEntityFacade() {
        super(StaffgroupEntity.class);
    }
}
