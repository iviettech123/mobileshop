/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.ProvincecitiesEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface ProvincecitiesEntityFacadeLocal {

    void create(ProvincecitiesEntity provincecitiesEntity);

    void edit(ProvincecitiesEntity provincecitiesEntity);

    void remove(ProvincecitiesEntity provincecitiesEntity);

    ProvincecitiesEntity find(Object id);

    List<ProvincecitiesEntity> findAll();

    List<ProvincecitiesEntity> findRange(int[] range);

    int count();
    
    List<ProvincecitiesEntity> findAll2();
    
}
