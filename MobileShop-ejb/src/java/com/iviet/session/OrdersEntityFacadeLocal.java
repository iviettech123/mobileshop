/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.OrdersEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface OrdersEntityFacadeLocal {

    void create(OrdersEntity ordersEntity);

    void edit(OrdersEntity ordersEntity);

    void remove(OrdersEntity ordersEntity);

    OrdersEntity find(Object id);

    List<OrdersEntity> findAll();

    List<OrdersEntity> findRange(int[] range);

    int count();
    
}
