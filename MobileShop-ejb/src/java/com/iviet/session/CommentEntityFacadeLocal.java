/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.CommentEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface CommentEntityFacadeLocal {

    void create(CommentEntity commentEntity);

    void edit(CommentEntity commentEntity);

    void remove(CommentEntity commentEntity);

    CommentEntity find(Object id);

    List<CommentEntity> findAll();

    List<CommentEntity> findRange(int[] range);

    int count();
    
}
