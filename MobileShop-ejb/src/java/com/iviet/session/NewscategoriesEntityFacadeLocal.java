/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.NewscategoriesEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface NewscategoriesEntityFacadeLocal {

    void create(NewscategoriesEntity newscategoriesEntity);

    void edit(NewscategoriesEntity newscategoriesEntity);

    void remove(NewscategoriesEntity newscategoriesEntity);

    NewscategoriesEntity find(Object id);

    List<NewscategoriesEntity> findAll();

    List<NewscategoriesEntity> findRange(int[] range);

    int count();
    
}
