/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.ShipmentEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface ShipmentEntityFacadeLocal {

    void create(ShipmentEntity shipmentEntity);

    void edit(ShipmentEntity shipmentEntity);

    void remove(ShipmentEntity shipmentEntity);

    ShipmentEntity find(Object id);

    List<ShipmentEntity> findAll();

    List<ShipmentEntity> findRange(int[] range);

    int count();
    
}
