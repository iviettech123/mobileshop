/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.CategoriesEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author XuânBá
 */
@Stateless
public class CategoriesEntityFacade extends AbstractFacade<CategoriesEntity> implements CategoriesEntityFacadeLocal {

    @PersistenceContext(unitName = "MobileShop-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoriesEntityFacade() {
        super(CategoriesEntity.class);
    }

    public List<CategoriesEntity> findByName(String name){
        Query query = em.createNamedQuery("CategoriesEntity.findByName");
        query.setParameter("name", "%"+ name +"%");
        return query.getResultList();
    }
    

}
