/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.StaffEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface StaffEntityFacadeLocal {

    void create(StaffEntity staffEntity);

    void edit(StaffEntity staffEntity);

    void remove(StaffEntity staffEntity);

    StaffEntity find(Object id);

    List<StaffEntity> findAll();

    List<StaffEntity> findRange(int[] range);

    int count();
    
}
