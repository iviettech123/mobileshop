/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.CountriesEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface CountriesEntityFacadeLocal {

    void create(CountriesEntity countriesEntity);

    void edit(CountriesEntity countriesEntity);

    void remove(CountriesEntity countriesEntity);

    CountriesEntity find(Object id);

    List<CountriesEntity> findAll();

    List<CountriesEntity> findRange(int[] range);

    int count();
    
}
