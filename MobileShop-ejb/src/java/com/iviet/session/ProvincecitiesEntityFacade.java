/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.ProvincecitiesEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author XuânBá
 */
@Stateless
public class ProvincecitiesEntityFacade extends AbstractFacade<ProvincecitiesEntity> implements ProvincecitiesEntityFacadeLocal {
    @PersistenceContext(unitName = "MobileShop-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProvincecitiesEntityFacade() {
        super(ProvincecitiesEntity.class);
    }
    
    @Override
    public List<ProvincecitiesEntity> findAll2(){
        Query query = em.createNamedQuery("ProvincecitiesEntity.findAll");
        return query.getResultList();
    };
    
}
