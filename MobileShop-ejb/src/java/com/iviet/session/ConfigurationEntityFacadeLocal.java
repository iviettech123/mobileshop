/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.ConfigurationEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface ConfigurationEntityFacadeLocal {

    void create(ConfigurationEntity configurationEntity);

    void edit(ConfigurationEntity configurationEntity);

    void remove(ConfigurationEntity configurationEntity);

    ConfigurationEntity find(Object id);

    List<ConfigurationEntity> findAll();

    List<ConfigurationEntity> findRange(int[] range);

    int count();
    
}
