/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.ShipmentEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author XuânBá
 */
@Stateless
public class ShipmentEntityFacade extends AbstractFacade<ShipmentEntity> implements ShipmentEntityFacadeLocal {
    @PersistenceContext(unitName = "MobileShop-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ShipmentEntityFacade() {
        super(ShipmentEntity.class);
    }
    
}
