/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.OrderdetailsEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface OrderdetailsEntityFacadeLocal {

    void create(OrderdetailsEntity orderdetailsEntity);

    void edit(OrderdetailsEntity orderdetailsEntity);

    void remove(OrderdetailsEntity orderdetailsEntity);

    OrderdetailsEntity find(Object id);

    List<OrderdetailsEntity> findAll();

    List<OrderdetailsEntity> findRange(int[] range);

    int count();
    
}
