/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.ImagesEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author XuânBá
 */
@Stateless
public class ImagesEntityFacade extends AbstractFacade<ImagesEntity> implements ImagesEntityFacadeLocal {
    @PersistenceContext(unitName = "MobileShop-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ImagesEntityFacade() {
        super(ImagesEntity.class);
    }
    
}
