/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.CategoriesEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface CategoriesEntityFacadeLocal {

    void create(CategoriesEntity categoriesEntity);

    void edit(CategoriesEntity categoriesEntity);

    void remove(CategoriesEntity categoriesEntity);

    CategoriesEntity find(Object id);

    List<CategoriesEntity> findAll();

    List<CategoriesEntity> findRange(int[] range);

    int count();
    
    List<CategoriesEntity> findByName(String name);
    
}
