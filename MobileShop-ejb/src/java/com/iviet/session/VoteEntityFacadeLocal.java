/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.VoteEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface VoteEntityFacadeLocal {

    void create(VoteEntity voteEntity);

    void edit(VoteEntity voteEntity);

    void remove(VoteEntity voteEntity);

    VoteEntity find(Object id);

    List<VoteEntity> findAll();

    List<VoteEntity> findRange(int[] range);

    int count();
    
}
