/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.NewscategoriesEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author XuânBá
 */
@Stateless
public class NewscategoriesEntityFacade extends AbstractFacade<NewscategoriesEntity> implements NewscategoriesEntityFacadeLocal {
    @PersistenceContext(unitName = "MobileShop-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NewscategoriesEntityFacade() {
        super(NewscategoriesEntity.class);
    }
    
}
