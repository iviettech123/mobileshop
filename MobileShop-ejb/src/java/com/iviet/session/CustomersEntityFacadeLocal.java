/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.CustomersEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface CustomersEntityFacadeLocal {

    void create(CustomersEntity customersEntity);

    void edit(CustomersEntity customersEntity);

    void remove(CustomersEntity customersEntity);

    CustomersEntity find(Object id);

    List<CustomersEntity> findAll();

    List<CustomersEntity> findRange(int[] range);

    int count();
    
}
