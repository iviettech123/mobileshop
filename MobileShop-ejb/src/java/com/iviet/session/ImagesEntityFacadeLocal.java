/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.session;

import com.iviet.entity.ImagesEntity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author XuânBá
 */
@Local
public interface ImagesEntityFacadeLocal {

    void create(ImagesEntity imagesEntity);

    void edit(ImagesEntity imagesEntity);

    void remove(ImagesEntity imagesEntity);

    ImagesEntity find(Object id);

    List<ImagesEntity> findAll();

    List<ImagesEntity> findRange(int[] range);

    int count();
    
}
