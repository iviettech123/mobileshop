/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "orders")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrdersEntity.findAll", query = "SELECT o FROM OrdersEntity o"),
    @NamedQuery(name = "OrdersEntity.findByOrderId", query = "SELECT o FROM OrdersEntity o WHERE o.orderId = :orderId"),
    @NamedQuery(name = "OrdersEntity.findByDate", query = "SELECT o FROM OrdersEntity o WHERE o.date = :date"),
    @NamedQuery(name = "OrdersEntity.findByStatus", query = "SELECT o FROM OrdersEntity o WHERE o.status = :status"),
    @NamedQuery(name = "OrdersEntity.findByTotal", query = "SELECT o FROM OrdersEntity o WHERE o.total = :total")})
public class OrdersEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "OrderId")
    private String orderId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Size(max = 100)
    @Column(name = "Status")
    private String status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Total")
    private Double total;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orderId")
    private Collection<OrderdetailsEntity> orderdetailsEntityCollection;
    @JoinColumn(name = "CustomerId", referencedColumnName = "CustomerId")
    @ManyToOne(optional = false)
    private CustomersEntity customerId;
    @JoinColumn(name = "PaymentId", referencedColumnName = "PaymentId")
    @ManyToOne(optional = false)
    private PaymentEntity paymentId;
    @JoinColumn(name = "ShipmentId", referencedColumnName = "ShipmentId")
    @ManyToOne(optional = false)
    private ShipmentEntity shipmentId;

    public OrdersEntity() {
    }

    public OrdersEntity(String orderId) {
        this.orderId = orderId;
    }

    public OrdersEntity(String orderId, Date date) {
        this.orderId = orderId;
        this.date = date;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @XmlTransient
    public Collection<OrderdetailsEntity> getOrderdetailsEntityCollection() {
        return orderdetailsEntityCollection;
    }

    public void setOrderdetailsEntityCollection(Collection<OrderdetailsEntity> orderdetailsEntityCollection) {
        this.orderdetailsEntityCollection = orderdetailsEntityCollection;
    }

    public CustomersEntity getCustomerId() {
        return customerId;
    }

    public void setCustomerId(CustomersEntity customerId) {
        this.customerId = customerId;
    }

    public PaymentEntity getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(PaymentEntity paymentId) {
        this.paymentId = paymentId;
    }

    public ShipmentEntity getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(ShipmentEntity shipmentId) {
        this.shipmentId = shipmentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderId != null ? orderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdersEntity)) {
            return false;
        }
        OrdersEntity other = (OrdersEntity) object;
        if ((this.orderId == null && other.orderId != null) || (this.orderId != null && !this.orderId.equals(other.orderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.OrdersEntity[ orderId=" + orderId + " ]";
    }
    
}
