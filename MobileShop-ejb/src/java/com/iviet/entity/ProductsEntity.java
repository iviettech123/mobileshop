/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "products")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductsEntity.findAll", query = "SELECT p FROM ProductsEntity p"),
    @NamedQuery(name = "ProductsEntity.findByProductId", query = "SELECT p FROM ProductsEntity p WHERE p.productId = :productId"),
    @NamedQuery(name = "ProductsEntity.findByName", query = "SELECT p FROM ProductsEntity p WHERE p.name = :name"),
    @NamedQuery(name = "ProductsEntity.findByPrice", query = "SELECT p FROM ProductsEntity p WHERE p.price = :price"),
    @NamedQuery(name = "ProductsEntity.findByPromotions", query = "SELECT p FROM ProductsEntity p WHERE p.promotions = :promotions"),
    @NamedQuery(name = "ProductsEntity.findByPublic1", query = "SELECT p FROM ProductsEntity p WHERE p.public1 = :public1"),
    @NamedQuery(name = "ProductsEntity.findByAvailable", query = "SELECT p FROM ProductsEntity p WHERE p.available = :available"),
    @NamedQuery(name = "ProductsEntity.findByHomePage", query = "SELECT p FROM ProductsEntity p WHERE p.homePage = :homePage"),
    @NamedQuery(name = "ProductsEntity.findByImageThumb", query = "SELECT p FROM ProductsEntity p WHERE p.imageThumb = :imageThumb")})
public class ProductsEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ProductId")
    private String productId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Price")
    private double price;
    @Size(max = 300)
    @Column(name = "Promotions")
    private String promotions;
    @Lob
    @Size(max = 65535)
    @Column(name = "Description")
    private String description;
    @Column(name = "Public")
    private Short public1;
    @Size(max = 100)
    @Column(name = "Available")
    private String available;
    @Column(name = "HomePage")
    private Short homePage;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ImageThumb")
    private String imageThumb;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    private Collection<ImagesEntity> imagesEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    private Collection<ConfigurationEntity> configurationEntityCollection;
    @JoinColumn(name = "CategoryId", referencedColumnName = "CategoryId")
    @ManyToOne(optional = false)
    private CategoriesEntity categoryId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    private Collection<OrderdetailsEntity> orderdetailsEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    private Collection<CommentEntity> commentEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productId")
    private Collection<VoteEntity> voteEntityCollection;

    public ProductsEntity() {
    }

    public ProductsEntity(String productId) {
        this.productId = productId;
    }

    public ProductsEntity(String productId, String name, double price, String imageThumb) {
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.imageThumb = imageThumb;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPromotions() {
        return promotions;
    }

    public void setPromotions(String promotions) {
        this.promotions = promotions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Short getPublic1() {
        return public1;
    }

    public void setPublic1(Short public1) {
        this.public1 = public1;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public Short getHomePage() {
        return homePage;
    }

    public void setHomePage(Short homePage) {
        this.homePage = homePage;
    }

    public String getImageThumb() {
        return imageThumb;
    }

    public void setImageThumb(String imageThumb) {
        this.imageThumb = imageThumb;
    }

    @XmlTransient
    public Collection<ImagesEntity> getImagesEntityCollection() {
        return imagesEntityCollection;
    }

    public void setImagesEntityCollection(Collection<ImagesEntity> imagesEntityCollection) {
        this.imagesEntityCollection = imagesEntityCollection;
    }

    @XmlTransient
    public Collection<ConfigurationEntity> getConfigurationEntityCollection() {
        return configurationEntityCollection;
    }

    public void setConfigurationEntityCollection(Collection<ConfigurationEntity> configurationEntityCollection) {
        this.configurationEntityCollection = configurationEntityCollection;
    }

    public CategoriesEntity getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(CategoriesEntity categoryId) {
        this.categoryId = categoryId;
    }

    @XmlTransient
    public Collection<OrderdetailsEntity> getOrderdetailsEntityCollection() {
        return orderdetailsEntityCollection;
    }

    public void setOrderdetailsEntityCollection(Collection<OrderdetailsEntity> orderdetailsEntityCollection) {
        this.orderdetailsEntityCollection = orderdetailsEntityCollection;
    }

    @XmlTransient
    public Collection<CommentEntity> getCommentEntityCollection() {
        return commentEntityCollection;
    }

    public void setCommentEntityCollection(Collection<CommentEntity> commentEntityCollection) {
        this.commentEntityCollection = commentEntityCollection;
    }

    @XmlTransient
    public Collection<VoteEntity> getVoteEntityCollection() {
        return voteEntityCollection;
    }

    public void setVoteEntityCollection(Collection<VoteEntity> voteEntityCollection) {
        this.voteEntityCollection = voteEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productId != null ? productId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductsEntity)) {
            return false;
        }
        ProductsEntity other = (ProductsEntity) object;
        if ((this.productId == null && other.productId != null) || (this.productId != null && !this.productId.equals(other.productId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.ProductsEntity[ productId=" + productId + " ]";
    }
    
}
