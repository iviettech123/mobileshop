/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "news")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NewsEntity.findAll", query = "SELECT n FROM NewsEntity n"),
    @NamedQuery(name = "NewsEntity.findByNewsId", query = "SELECT n FROM NewsEntity n WHERE n.newsId = :newsId"),
    @NamedQuery(name = "NewsEntity.findByTitle", query = "SELECT n FROM NewsEntity n WHERE n.title = :title"),
    @NamedQuery(name = "NewsEntity.findByImageThumb", query = "SELECT n FROM NewsEntity n WHERE n.imageThumb = :imageThumb"),
    @NamedQuery(name = "NewsEntity.findByStaffId", query = "SELECT n FROM NewsEntity n WHERE n.staffId = :staffId")})
public class NewsEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "NewsId")
    private Integer newsId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "Content")
    private String content;
    @Size(max = 100)
    @Column(name = "ImageThumb")
    private String imageThumb;
    @Basic(optional = false)
    @NotNull
    @Column(name = "StaffId")
    private int staffId;
    @JoinColumn(name = "NewsCategoryId", referencedColumnName = "NewsCategoryId")
    @ManyToOne(optional = false)
    private NewscategoriesEntity newsCategoryId;

    public NewsEntity() {
    }

    public NewsEntity(Integer newsId) {
        this.newsId = newsId;
    }

    public NewsEntity(Integer newsId, String title, String content, int staffId) {
        this.newsId = newsId;
        this.title = title;
        this.content = content;
        this.staffId = staffId;
    }

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageThumb() {
        return imageThumb;
    }

    public void setImageThumb(String imageThumb) {
        this.imageThumb = imageThumb;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public NewscategoriesEntity getNewsCategoryId() {
        return newsCategoryId;
    }

    public void setNewsCategoryId(NewscategoriesEntity newsCategoryId) {
        this.newsCategoryId = newsCategoryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (newsId != null ? newsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsEntity)) {
            return false;
        }
        NewsEntity other = (NewsEntity) object;
        if ((this.newsId == null && other.newsId != null) || (this.newsId != null && !this.newsId.equals(other.newsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.NewsEntity[ newsId=" + newsId + " ]";
    }
    
}
