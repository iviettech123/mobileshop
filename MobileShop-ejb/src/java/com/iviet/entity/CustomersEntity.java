/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "customers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomersEntity.findAll", query = "SELECT c FROM CustomersEntity c"),
    @NamedQuery(name = "CustomersEntity.findByCustomerId", query = "SELECT c FROM CustomersEntity c WHERE c.customerId = :customerId"),
    @NamedQuery(name = "CustomersEntity.findByName", query = "SELECT c FROM CustomersEntity c WHERE c.name = :name"),
    @NamedQuery(name = "CustomersEntity.findByEmail", query = "SELECT c FROM CustomersEntity c WHERE c.email = :email"),
    @NamedQuery(name = "CustomersEntity.findByPhone", query = "SELECT c FROM CustomersEntity c WHERE c.phone = :phone"),
    @NamedQuery(name = "CustomersEntity.findByGender", query = "SELECT c FROM CustomersEntity c WHERE c.gender = :gender"),
    @NamedQuery(name = "CustomersEntity.findByPassword", query = "SELECT c FROM CustomersEntity c WHERE c.password = :password"),
    @NamedQuery(name = "CustomersEntity.findByBirthday", query = "SELECT c FROM CustomersEntity c WHERE c.birthday = :birthday"),
    @NamedQuery(name = "CustomersEntity.findByRegisterDate", query = "SELECT c FROM CustomersEntity c WHERE c.registerDate = :registerDate"),
    @NamedQuery(name = "CustomersEntity.findByLastAccessDate", query = "SELECT c FROM CustomersEntity c WHERE c.lastAccessDate = :lastAccessDate"),
    @NamedQuery(name = "CustomersEntity.findByStatus", query = "SELECT c FROM CustomersEntity c WHERE c.status = :status")})
public class CustomersEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CustomerId")
    private String customerId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Name")
    private String name;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "Email")
    private String email;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "Phone")
    private String phone;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Gender")
    private short gender;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Password")
    private String password;
    @Column(name = "Birthday")
    @Temporal(TemporalType.DATE)
    private Date birthday;
    @Column(name = "RegisterDate")
    @Temporal(TemporalType.DATE)
    private Date registerDate;
    @Column(name = "LastAccessDate")
    @Temporal(TemporalType.DATE)
    private Date lastAccessDate;
    @Size(max = 20)
    @Column(name = "Status")
    private String status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerId")
    private Collection<CommentEntity> commentEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerId")
    private Collection<OrdersEntity> ordersEntityCollection;
    @JoinColumn(name = "AddressId", referencedColumnName = "AddressId")
    @ManyToOne(optional = false)
    private AddressesEntity addressId;

    public CustomersEntity() {
    }

    public CustomersEntity(String customerId) {
        this.customerId = customerId;
    }

    public CustomersEntity(String customerId, String name, String phone, short gender, String password) {
        this.customerId = customerId;
        this.name = name;
        this.phone = phone;
        this.gender = gender;
        this.password = password;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public short getGender() {
        return gender;
    }

    public void setGender(short gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public Date getLastAccessDate() {
        return lastAccessDate;
    }

    public void setLastAccessDate(Date lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<CommentEntity> getCommentEntityCollection() {
        return commentEntityCollection;
    }

    public void setCommentEntityCollection(Collection<CommentEntity> commentEntityCollection) {
        this.commentEntityCollection = commentEntityCollection;
    }

    @XmlTransient
    public Collection<OrdersEntity> getOrdersEntityCollection() {
        return ordersEntityCollection;
    }

    public void setOrdersEntityCollection(Collection<OrdersEntity> ordersEntityCollection) {
        this.ordersEntityCollection = ordersEntityCollection;
    }

    public AddressesEntity getAddressId() {
        return addressId;
    }

    public void setAddressId(AddressesEntity addressId) {
        this.addressId = addressId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customerId != null ? customerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomersEntity)) {
            return false;
        }
        CustomersEntity other = (CustomersEntity) object;
        if ((this.customerId == null && other.customerId != null) || (this.customerId != null && !this.customerId.equals(other.customerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.CustomersEntity[ customerId=" + customerId + " ]";
    }
    
}
