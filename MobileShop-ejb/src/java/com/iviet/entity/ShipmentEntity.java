/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "shipment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ShipmentEntity.findAll", query = "SELECT s FROM ShipmentEntity s"),
    @NamedQuery(name = "ShipmentEntity.findByShipmentId", query = "SELECT s FROM ShipmentEntity s WHERE s.shipmentId = :shipmentId"),
    @NamedQuery(name = "ShipmentEntity.findByMethod", query = "SELECT s FROM ShipmentEntity s WHERE s.method = :method")})
public class ShipmentEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ShipmentId")
    private Integer shipmentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Method")
    private String method;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "shipmentId")
    private Collection<OrdersEntity> ordersEntityCollection;

    public ShipmentEntity() {
    }

    public ShipmentEntity(Integer shipmentId) {
        this.shipmentId = shipmentId;
    }

    public ShipmentEntity(Integer shipmentId, String method) {
        this.shipmentId = shipmentId;
        this.method = method;
    }

    public Integer getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(Integer shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @XmlTransient
    public Collection<OrdersEntity> getOrdersEntityCollection() {
        return ordersEntityCollection;
    }

    public void setOrdersEntityCollection(Collection<OrdersEntity> ordersEntityCollection) {
        this.ordersEntityCollection = ordersEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shipmentId != null ? shipmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ShipmentEntity)) {
            return false;
        }
        ShipmentEntity other = (ShipmentEntity) object;
        if ((this.shipmentId == null && other.shipmentId != null) || (this.shipmentId != null && !this.shipmentId.equals(other.shipmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.ShipmentEntity[ shipmentId=" + shipmentId + " ]";
    }
    
}
