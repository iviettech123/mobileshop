/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "staff")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StaffEntity.findAll", query = "SELECT s FROM StaffEntity s"),
    @NamedQuery(name = "StaffEntity.findByStaffId", query = "SELECT s FROM StaffEntity s WHERE s.staffId = :staffId"),
    @NamedQuery(name = "StaffEntity.findByName", query = "SELECT s FROM StaffEntity s WHERE s.name = :name"),
    @NamedQuery(name = "StaffEntity.findByCreateDay", query = "SELECT s FROM StaffEntity s WHERE s.createDay = :createDay"),
    @NamedQuery(name = "StaffEntity.findByActive", query = "SELECT s FROM StaffEntity s WHERE s.active = :active"),
    @NamedQuery(name = "StaffEntity.findByUsername", query = "SELECT s FROM StaffEntity s WHERE s.username = :username"),
    @NamedQuery(name = "StaffEntity.findByPassword", query = "SELECT s FROM StaffEntity s WHERE s.password = :password"),
    @NamedQuery(name = "StaffEntity.findByEmail", query = "SELECT s FROM StaffEntity s WHERE s.email = :email")})
public class StaffEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "StaffId")
    private Integer staffId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Name")
    private String name;
    @Column(name = "CreateDay")
    @Temporal(TemporalType.DATE)
    private Date createDay;
    @Column(name = "Active")
    private Short active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Password")
    private String password;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "Email")
    private String email;
    @JoinColumn(name = "GroupId", referencedColumnName = "GroupId")
    @ManyToOne(optional = false)
    private StaffgroupEntity groupId;

    public StaffEntity() {
    }

    public StaffEntity(Integer staffId) {
        this.staffId = staffId;
    }

    public StaffEntity(Integer staffId, String name, String username, String password) {
        this.staffId = staffId;
        this.name = name;
        this.username = username;
        this.password = password;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDay() {
        return createDay;
    }

    public void setCreateDay(Date createDay) {
        this.createDay = createDay;
    }

    public Short getActive() {
        return active;
    }

    public void setActive(Short active) {
        this.active = active;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public StaffgroupEntity getGroupId() {
        return groupId;
    }

    public void setGroupId(StaffgroupEntity groupId) {
        this.groupId = groupId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (staffId != null ? staffId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StaffEntity)) {
            return false;
        }
        StaffEntity other = (StaffEntity) object;
        if ((this.staffId == null && other.staffId != null) || (this.staffId != null && !this.staffId.equals(other.staffId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.StaffEntity[ staffId=" + staffId + " ]";
    }
    
}
