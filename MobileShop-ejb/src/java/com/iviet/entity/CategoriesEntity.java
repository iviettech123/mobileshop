/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "categories")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CategoriesEntity.findAll", query = "SELECT c FROM CategoriesEntity c"),
    @NamedQuery(name = "CategoriesEntity.findByCategoryId", query = "SELECT c FROM CategoriesEntity c WHERE c.categoryId = :categoryId"),
    @NamedQuery(name = "CategoriesEntity.findByName", query = "SELECT c FROM CategoriesEntity c WHERE c.name LIKE :name")})
public class CategoriesEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CategoryId")
    private Integer categoryId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryId")
    private Collection<AccessoriesEntity> accessoriesEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryId")
    private Collection<ProductsEntity> productsEntityCollection;

    public CategoriesEntity() {
    }

    public CategoriesEntity(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public CategoriesEntity(Integer categoryId, String name) {
        this.categoryId = categoryId;
        this.name = name;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<AccessoriesEntity> getAccessoriesEntityCollection() {
        return accessoriesEntityCollection;
    }

    public void setAccessoriesEntityCollection(Collection<AccessoriesEntity> accessoriesEntityCollection) {
        this.accessoriesEntityCollection = accessoriesEntityCollection;
    }

    @XmlTransient
    public Collection<ProductsEntity> getProductsEntityCollection() {
        return productsEntityCollection;
    }

    public void setProductsEntityCollection(Collection<ProductsEntity> productsEntityCollection) {
        this.productsEntityCollection = productsEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoryId != null ? categoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriesEntity)) {
            return false;
        }
        CategoriesEntity other = (CategoriesEntity) object;
        if ((this.categoryId == null && other.categoryId != null) || (this.categoryId != null && !this.categoryId.equals(other.categoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.CategoriesEntity[ categoryId=" + categoryId + " ]";
    }
    
}
