/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "addresses")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AddressesEntity.findAll", query = "SELECT a FROM AddressesEntity a"),
    @NamedQuery(name = "AddressesEntity.findByAddressId", query = "SELECT a FROM AddressesEntity a WHERE a.addressId = :addressId"),
    @NamedQuery(name = "AddressesEntity.findByStreet", query = "SELECT a FROM AddressesEntity a WHERE a.street = :street"),
    @NamedQuery(name = "AddressesEntity.findByTownship", query = "SELECT a FROM AddressesEntity a WHERE a.township = :township")})
public class AddressesEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "AddressId")
    private Integer addressId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Street")
    private String street;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Township")
    private String township;
    @JoinColumn(name = "ProvinceId", referencedColumnName = "ProvinceId")
    @ManyToOne(optional = false)
    private ProvincecitiesEntity provinceId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "addressId")
    private Collection<CustomersEntity> customersEntityCollection;

    public AddressesEntity() {
    }

    public AddressesEntity(Integer addressId) {
        this.addressId = addressId;
    }

    public AddressesEntity(Integer addressId, String street, String township) {
        this.addressId = addressId;
        this.street = street;
        this.township = township;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTownship() {
        return township;
    }

    public void setTownship(String township) {
        this.township = township;
    }

    public ProvincecitiesEntity getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(ProvincecitiesEntity provinceId) {
        this.provinceId = provinceId;
    }

    @XmlTransient
    public Collection<CustomersEntity> getCustomersEntityCollection() {
        return customersEntityCollection;
    }

    public void setCustomersEntityCollection(Collection<CustomersEntity> customersEntityCollection) {
        this.customersEntityCollection = customersEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (addressId != null ? addressId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AddressesEntity)) {
            return false;
        }
        AddressesEntity other = (AddressesEntity) object;
        if ((this.addressId == null && other.addressId != null) || (this.addressId != null && !this.addressId.equals(other.addressId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.AddressesEntity[ addressId=" + addressId + " ]";
    }
    
}
