/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "staffgroup")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StaffgroupEntity.findAll", query = "SELECT s FROM StaffgroupEntity s"),
    @NamedQuery(name = "StaffgroupEntity.findByGroupId", query = "SELECT s FROM StaffgroupEntity s WHERE s.groupId = :groupId"),
    @NamedQuery(name = "StaffgroupEntity.findByName", query = "SELECT s FROM StaffgroupEntity s WHERE s.name = :name")})
public class StaffgroupEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GroupId")
    private Integer groupId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "groupId")
    private Collection<StaffEntity> staffEntityCollection;

    public StaffgroupEntity() {
    }

    public StaffgroupEntity(Integer groupId) {
        this.groupId = groupId;
    }

    public StaffgroupEntity(Integer groupId, String name) {
        this.groupId = groupId;
        this.name = name;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<StaffEntity> getStaffEntityCollection() {
        return staffEntityCollection;
    }

    public void setStaffEntityCollection(Collection<StaffEntity> staffEntityCollection) {
        this.staffEntityCollection = staffEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupId != null ? groupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StaffgroupEntity)) {
            return false;
        }
        StaffgroupEntity other = (StaffgroupEntity) object;
        if ((this.groupId == null && other.groupId != null) || (this.groupId != null && !this.groupId.equals(other.groupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.StaffgroupEntity[ groupId=" + groupId + " ]";
    }
    
}
