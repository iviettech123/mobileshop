/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "payment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentEntity.findAll", query = "SELECT p FROM PaymentEntity p"),
    @NamedQuery(name = "PaymentEntity.findByPaymentId", query = "SELECT p FROM PaymentEntity p WHERE p.paymentId = :paymentId"),
    @NamedQuery(name = "PaymentEntity.findByMethod", query = "SELECT p FROM PaymentEntity p WHERE p.method = :method")})
public class PaymentEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PaymentId")
    private Integer paymentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Method")
    private String method;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paymentId")
    private Collection<OrdersEntity> ordersEntityCollection;

    public PaymentEntity() {
    }

    public PaymentEntity(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public PaymentEntity(Integer paymentId, String method) {
        this.paymentId = paymentId;
        this.method = method;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @XmlTransient
    public Collection<OrdersEntity> getOrdersEntityCollection() {
        return ordersEntityCollection;
    }

    public void setOrdersEntityCollection(Collection<OrdersEntity> ordersEntityCollection) {
        this.ordersEntityCollection = ordersEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paymentId != null ? paymentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentEntity)) {
            return false;
        }
        PaymentEntity other = (PaymentEntity) object;
        if ((this.paymentId == null && other.paymentId != null) || (this.paymentId != null && !this.paymentId.equals(other.paymentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.PaymentEntity[ paymentId=" + paymentId + " ]";
    }
    
}
