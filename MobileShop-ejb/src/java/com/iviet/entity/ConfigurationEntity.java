/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "configuration")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConfigurationEntity.findAll", query = "SELECT c FROM ConfigurationEntity c"),
    @NamedQuery(name = "ConfigurationEntity.findByConfigurationId", query = "SELECT c FROM ConfigurationEntity c WHERE c.configurationId = :configurationId"),
    @NamedQuery(name = "ConfigurationEntity.findByName", query = "SELECT c FROM ConfigurationEntity c WHERE c.name = :name")})
public class ConfigurationEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ConfigurationId")
    private Integer configurationId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "Value")
    private String value;
    @JoinColumn(name = "ProductId", referencedColumnName = "ProductId")
    @ManyToOne(optional = false)
    private ProductsEntity productId;

    public ConfigurationEntity() {
    }

    public ConfigurationEntity(Integer configurationId) {
        this.configurationId = configurationId;
    }

    public ConfigurationEntity(Integer configurationId, String name, String value) {
        this.configurationId = configurationId;
        this.name = name;
        this.value = value;
    }

    public Integer getConfigurationId() {
        return configurationId;
    }

    public void setConfigurationId(Integer configurationId) {
        this.configurationId = configurationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ProductsEntity getProductId() {
        return productId;
    }

    public void setProductId(ProductsEntity productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (configurationId != null ? configurationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfigurationEntity)) {
            return false;
        }
        ConfigurationEntity other = (ConfigurationEntity) object;
        if ((this.configurationId == null && other.configurationId != null) || (this.configurationId != null && !this.configurationId.equals(other.configurationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.ConfigurationEntity[ configurationId=" + configurationId + " ]";
    }
    
}
