/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "provincecities")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProvincecitiesEntity.findAll", query = "SELECT p FROM ProvincecitiesEntity p"),
    @NamedQuery(name = "ProvincecitiesEntity.findByProvinceId", query = "SELECT p FROM ProvincecitiesEntity p WHERE p.provinceId = :provinceId"),
    @NamedQuery(name = "ProvincecitiesEntity.findByName", query = "SELECT p FROM ProvincecitiesEntity p WHERE p.name = :name")})
public class ProvincecitiesEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ProvinceId")
    private Integer provinceId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "provinceId")
    private Collection<AddressesEntity> addressesEntityCollection;
    @JoinColumn(name = "CountryId", referencedColumnName = "CountryId")
    @ManyToOne(optional = false)
    private CountriesEntity countryId;

    public ProvincecitiesEntity() {
    }

    public ProvincecitiesEntity(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public ProvincecitiesEntity(Integer provinceId, String name) {
        this.provinceId = provinceId;
        this.name = name;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<AddressesEntity> getAddressesEntityCollection() {
        return addressesEntityCollection;
    }

    public void setAddressesEntityCollection(Collection<AddressesEntity> addressesEntityCollection) {
        this.addressesEntityCollection = addressesEntityCollection;
    }

    public CountriesEntity getCountryId() {
        return countryId;
    }

    public void setCountryId(CountriesEntity countryId) {
        this.countryId = countryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (provinceId != null ? provinceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProvincecitiesEntity)) {
            return false;
        }
        ProvincecitiesEntity other = (ProvincecitiesEntity) object;
        if ((this.provinceId == null && other.provinceId != null) || (this.provinceId != null && !this.provinceId.equals(other.provinceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.ProvincecitiesEntity[ provinceId=" + provinceId + " ]";
    }
    
}
