/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "orderdetails")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderdetailsEntity.findAll", query = "SELECT o FROM OrderdetailsEntity o"),
    @NamedQuery(name = "OrderdetailsEntity.findByOrderDetailId", query = "SELECT o FROM OrderdetailsEntity o WHERE o.orderDetailId = :orderDetailId"),
    @NamedQuery(name = "OrderdetailsEntity.findByUnitPrice", query = "SELECT o FROM OrderdetailsEntity o WHERE o.unitPrice = :unitPrice"),
    @NamedQuery(name = "OrderdetailsEntity.findByColor", query = "SELECT o FROM OrderdetailsEntity o WHERE o.color = :color"),
    @NamedQuery(name = "OrderdetailsEntity.findByQuantity", query = "SELECT o FROM OrderdetailsEntity o WHERE o.quantity = :quantity")})
public class OrderdetailsEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OrderDetailId")
    private Integer orderDetailId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UnitPrice")
    private double unitPrice;
    @Size(max = 100)
    @Column(name = "Color")
    private String color;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Quantity")
    private int quantity;
    @JoinColumn(name = "OrderId", referencedColumnName = "OrderId")
    @ManyToOne(optional = false)
    private OrdersEntity orderId;
    @JoinColumn(name = "ProductId", referencedColumnName = "ProductId")
    @ManyToOne(optional = false)
    private ProductsEntity productId;

    public OrderdetailsEntity() {
    }

    public OrderdetailsEntity(Integer orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public OrderdetailsEntity(Integer orderDetailId, double unitPrice, int quantity) {
        this.orderDetailId = orderDetailId;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }

    public Integer getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(Integer orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public OrdersEntity getOrderId() {
        return orderId;
    }

    public void setOrderId(OrdersEntity orderId) {
        this.orderId = orderId;
    }

    public ProductsEntity getProductId() {
        return productId;
    }

    public void setProductId(ProductsEntity productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderDetailId != null ? orderDetailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderdetailsEntity)) {
            return false;
        }
        OrderdetailsEntity other = (OrderdetailsEntity) object;
        if ((this.orderDetailId == null && other.orderDetailId != null) || (this.orderDetailId != null && !this.orderDetailId.equals(other.orderDetailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.OrderdetailsEntity[ orderDetailId=" + orderDetailId + " ]";
    }
    
}
