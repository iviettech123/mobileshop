/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "countries")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CountriesEntity.findAll", query = "SELECT c FROM CountriesEntity c"),
    @NamedQuery(name = "CountriesEntity.findByCountryId", query = "SELECT c FROM CountriesEntity c WHERE c.countryId = :countryId"),
    @NamedQuery(name = "CountriesEntity.findByName", query = "SELECT c FROM CountriesEntity c WHERE c.name = :name"),
    @NamedQuery(name = "CountriesEntity.findByCode", query = "SELECT c FROM CountriesEntity c WHERE c.code = :code")})
public class CountriesEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CountryId")
    private Integer countryId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Code")
    private String code;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "countryId")
    private Collection<ProvincecitiesEntity> provincecitiesEntityCollection;

    public CountriesEntity() {
    }

    public CountriesEntity(Integer countryId) {
        this.countryId = countryId;
    }

    public CountriesEntity(Integer countryId, String name, String code) {
        this.countryId = countryId;
        this.name = name;
        this.code = code;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlTransient
    public Collection<ProvincecitiesEntity> getProvincecitiesEntityCollection() {
        return provincecitiesEntityCollection;
    }

    public void setProvincecitiesEntityCollection(Collection<ProvincecitiesEntity> provincecitiesEntityCollection) {
        this.provincecitiesEntityCollection = provincecitiesEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (countryId != null ? countryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CountriesEntity)) {
            return false;
        }
        CountriesEntity other = (CountriesEntity) object;
        if ((this.countryId == null && other.countryId != null) || (this.countryId != null && !this.countryId.equals(other.countryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.CountriesEntity[ countryId=" + countryId + " ]";
    }
    
}
