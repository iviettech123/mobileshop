/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "newscategories")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NewscategoriesEntity.findAll", query = "SELECT n FROM NewscategoriesEntity n"),
    @NamedQuery(name = "NewscategoriesEntity.findByNewsCategoryId", query = "SELECT n FROM NewscategoriesEntity n WHERE n.newsCategoryId = :newsCategoryId"),
    @NamedQuery(name = "NewscategoriesEntity.findByName", query = "SELECT n FROM NewscategoriesEntity n WHERE n.name = :name")})
public class NewscategoriesEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "NewsCategoryId")
    private Integer newsCategoryId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "newsCategoryId")
    private Collection<NewsEntity> newsEntityCollection;

    public NewscategoriesEntity() {
    }

    public NewscategoriesEntity(Integer newsCategoryId) {
        this.newsCategoryId = newsCategoryId;
    }

    public NewscategoriesEntity(Integer newsCategoryId, String name) {
        this.newsCategoryId = newsCategoryId;
        this.name = name;
    }

    public Integer getNewsCategoryId() {
        return newsCategoryId;
    }

    public void setNewsCategoryId(Integer newsCategoryId) {
        this.newsCategoryId = newsCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<NewsEntity> getNewsEntityCollection() {
        return newsEntityCollection;
    }

    public void setNewsEntityCollection(Collection<NewsEntity> newsEntityCollection) {
        this.newsEntityCollection = newsEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (newsCategoryId != null ? newsCategoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewscategoriesEntity)) {
            return false;
        }
        NewscategoriesEntity other = (NewscategoriesEntity) object;
        if ((this.newsCategoryId == null && other.newsCategoryId != null) || (this.newsCategoryId != null && !this.newsCategoryId.equals(other.newsCategoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.NewscategoriesEntity[ newsCategoryId=" + newsCategoryId + " ]";
    }
    
}
