/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author XuânBá
 */
@Entity
@Table(name = "accessories")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccessoriesEntity.findAll", query = "SELECT a FROM AccessoriesEntity a"),
    @NamedQuery(name = "AccessoriesEntity.findByAccessoriesId", query = "SELECT a FROM AccessoriesEntity a WHERE a.accessoriesId = :accessoriesId"),
    @NamedQuery(name = "AccessoriesEntity.findByName", query = "SELECT a FROM AccessoriesEntity a WHERE a.name = :name"),
    @NamedQuery(name = "AccessoriesEntity.findByImageThumb", query = "SELECT a FROM AccessoriesEntity a WHERE a.imageThumb = :imageThumb"),
    @NamedQuery(name = "AccessoriesEntity.findByImage", query = "SELECT a FROM AccessoriesEntity a WHERE a.image = :image"),
    @NamedQuery(name = "AccessoriesEntity.findByPrice", query = "SELECT a FROM AccessoriesEntity a WHERE a.price = :price")})
public class AccessoriesEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AccessoriesId")
    private Integer accessoriesId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ImageThumb")
    private String imageThumb;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Image")
    private String image;
    @Lob
    @Size(max = 65535)
    @Column(name = "Description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Price")
    private double price;
    @JoinColumn(name = "CategoryId", referencedColumnName = "CategoryId")
    @ManyToOne(optional = false)
    private CategoriesEntity categoryId;

    public AccessoriesEntity() {
    }

    public AccessoriesEntity(Integer accessoriesId) {
        this.accessoriesId = accessoriesId;
    }

    public AccessoriesEntity(Integer accessoriesId, String name, String imageThumb, String image, double price) {
        this.accessoriesId = accessoriesId;
        this.name = name;
        this.imageThumb = imageThumb;
        this.image = image;
        this.price = price;
    }

    public Integer getAccessoriesId() {
        return accessoriesId;
    }

    public void setAccessoriesId(Integer accessoriesId) {
        this.accessoriesId = accessoriesId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageThumb() {
        return imageThumb;
    }

    public void setImageThumb(String imageThumb) {
        this.imageThumb = imageThumb;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public CategoriesEntity getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(CategoriesEntity categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accessoriesId != null ? accessoriesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccessoriesEntity)) {
            return false;
        }
        AccessoriesEntity other = (AccessoriesEntity) object;
        if ((this.accessoriesId == null && other.accessoriesId != null) || (this.accessoriesId != null && !this.accessoriesId.equals(other.accessoriesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iviet.entity.AccessoriesEntity[ accessoriesId=" + accessoriesId + " ]";
    }
    
}
