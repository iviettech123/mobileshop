/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.NewscategoriesEntity;
import com.iviet.session.NewscategoriesEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class NewscategoriesEntityFacadeStub {
    NewscategoriesEntityFacadeLocal newscategoriesEntityFacade = lookupNewscategoriesEntityFacadeLocal();

    private NewscategoriesEntityFacadeLocal lookupNewscategoriesEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (NewscategoriesEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/NewscategoriesEntityFacade!com.iviet.session.NewscategoriesEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(NewscategoriesEntity newscategoriesEntity) {
        newscategoriesEntityFacade.create(newscategoriesEntity);
    }

    public void edit(NewscategoriesEntity newscategoriesEntity) {
        newscategoriesEntityFacade.edit(newscategoriesEntity);
    }

    public void remove(NewscategoriesEntity newscategoriesEntity) {
        newscategoriesEntityFacade.remove(newscategoriesEntity);
    }

    public NewscategoriesEntity find(Object id) {
        return newscategoriesEntityFacade.find(id);
    }

    public List<NewscategoriesEntity> findAll() {
        return newscategoriesEntityFacade.findAll();
    }

    public List<NewscategoriesEntity> findRange(int[] range) {
        return newscategoriesEntityFacade.findRange(range);
    }

    public int count() {
        return newscategoriesEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return newscategoriesEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return newscategoriesEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return newscategoriesEntityFacade.toString();
    }
    
}
