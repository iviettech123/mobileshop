/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.CategoriesEntity;
import com.iviet.session.CategoriesEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class CategoriesEntityFacadeStub {
    CategoriesEntityFacadeLocal categoriesEntityFacade = lookupCategoriesEntityFacadeLocal();

    private CategoriesEntityFacadeLocal lookupCategoriesEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (CategoriesEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/CategoriesEntityFacade!com.iviet.session.CategoriesEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(CategoriesEntity categoriesEntity) {
        categoriesEntityFacade.create(categoriesEntity);
    }

    public void edit(CategoriesEntity categoriesEntity) {
        categoriesEntityFacade.edit(categoriesEntity);
    }

    public void remove(CategoriesEntity categoriesEntity) {
        categoriesEntityFacade.remove(categoriesEntity);
    }

    public CategoriesEntity find(Object id) {
        return categoriesEntityFacade.find(id);
    }

    public List<CategoriesEntity> findAll() {
        return categoriesEntityFacade.findAll();
    }
    

    public List<CategoriesEntity> findRange(int[] range) {
        return categoriesEntityFacade.findRange(range);
    }

    public int count() {
        return categoriesEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return categoriesEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return categoriesEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return categoriesEntityFacade.toString();
    }

    public List<CategoriesEntity> findByName(String name) {
        return categoriesEntityFacade.findByName(name);
    }
    
    

    

    
    
    
    
    
    

    
    
    
    
}
