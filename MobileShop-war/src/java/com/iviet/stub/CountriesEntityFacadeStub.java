/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.CountriesEntity;
import com.iviet.session.CountriesEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class CountriesEntityFacadeStub {
    CountriesEntityFacadeLocal countriesEntityFacade = lookupCountriesEntityFacadeLocal();

    private CountriesEntityFacadeLocal lookupCountriesEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (CountriesEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/CountriesEntityFacade!com.iviet.session.CountriesEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(CountriesEntity countriesEntity) {
        countriesEntityFacade.create(countriesEntity);
    }

    public void edit(CountriesEntity countriesEntity) {
        countriesEntityFacade.edit(countriesEntity);
    }

    public void remove(CountriesEntity countriesEntity) {
        countriesEntityFacade.remove(countriesEntity);
    }

    public CountriesEntity find(Object id) {
        return countriesEntityFacade.find(id);
    }

    public List<CountriesEntity> findAll() {
        return countriesEntityFacade.findAll();
    }

    public List<CountriesEntity> findRange(int[] range) {
        return countriesEntityFacade.findRange(range);
    }

    public int count() {
        return countriesEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return countriesEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return countriesEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return countriesEntityFacade.toString();
    }
    
}
