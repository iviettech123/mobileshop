/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.StaffEntity;
import com.iviet.session.StaffEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class StaffEntityFacadeStub {
    StaffEntityFacadeLocal staffEntityFacade = lookupStaffEntityFacadeLocal();

    private StaffEntityFacadeLocal lookupStaffEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (StaffEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/StaffEntityFacade!com.iviet.session.StaffEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(StaffEntity staffEntity) {
        staffEntityFacade.create(staffEntity);
    }

    public void edit(StaffEntity staffEntity) {
        staffEntityFacade.edit(staffEntity);
    }

    public void remove(StaffEntity staffEntity) {
        staffEntityFacade.remove(staffEntity);
    }

    public StaffEntity find(Object id) {
        return staffEntityFacade.find(id);
    }

    public List<StaffEntity> findAll() {
        return staffEntityFacade.findAll();
    }

    public List<StaffEntity> findRange(int[] range) {
        return staffEntityFacade.findRange(range);
    }

    public int count() {
        return staffEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return staffEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return staffEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return staffEntityFacade.toString();
    }
    
}
