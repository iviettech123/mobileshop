/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.PaymentEntity;
import com.iviet.session.PaymentEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class PaymentEntityFacadeStub {
    PaymentEntityFacadeLocal paymentEntityFacade = lookupPaymentEntityFacadeLocal();

    private PaymentEntityFacadeLocal lookupPaymentEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (PaymentEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/PaymentEntityFacade!com.iviet.session.PaymentEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(PaymentEntity paymentEntity) {
        paymentEntityFacade.create(paymentEntity);
    }

    public void edit(PaymentEntity paymentEntity) {
        paymentEntityFacade.edit(paymentEntity);
    }

    public void remove(PaymentEntity paymentEntity) {
        paymentEntityFacade.remove(paymentEntity);
    }

    public PaymentEntity find(Object id) {
        return paymentEntityFacade.find(id);
    }

    public List<PaymentEntity> findAll() {
        return paymentEntityFacade.findAll();
    }

    public List<PaymentEntity> findRange(int[] range) {
        return paymentEntityFacade.findRange(range);
    }

    public int count() {
        return paymentEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return paymentEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return paymentEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return paymentEntityFacade.toString();
    }
    
}
