/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.CommentEntity;
import com.iviet.session.CommentEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class CommentEntityFacadeStub {
    CommentEntityFacadeLocal commentEntityFacade = lookupCommentEntityFacadeLocal();

    private CommentEntityFacadeLocal lookupCommentEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (CommentEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/CommentEntityFacade!com.iviet.session.CommentEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(CommentEntity commentEntity) {
        commentEntityFacade.create(commentEntity);
    }

    public void edit(CommentEntity commentEntity) {
        commentEntityFacade.edit(commentEntity);
    }

    public void remove(CommentEntity commentEntity) {
        commentEntityFacade.remove(commentEntity);
    }

    public CommentEntity find(Object id) {
        return commentEntityFacade.find(id);
    }

    public List<CommentEntity> findAll() {
        return commentEntityFacade.findAll();
    }

    public List<CommentEntity> findRange(int[] range) {
        return commentEntityFacade.findRange(range);
    }

    public int count() {
        return commentEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return commentEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return commentEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return commentEntityFacade.toString();
    }
    
}
