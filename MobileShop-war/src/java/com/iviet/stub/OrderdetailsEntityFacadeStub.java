/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.OrderdetailsEntity;
import com.iviet.session.OrderdetailsEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class OrderdetailsEntityFacadeStub {
    OrderdetailsEntityFacadeLocal orderdetailsEntityFacade = lookupOrderdetailsEntityFacadeLocal();

    private OrderdetailsEntityFacadeLocal lookupOrderdetailsEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (OrderdetailsEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/OrderdetailsEntityFacade!com.iviet.session.OrderdetailsEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(OrderdetailsEntity orderdetailsEntity) {
        orderdetailsEntityFacade.create(orderdetailsEntity);
    }

    public void edit(OrderdetailsEntity orderdetailsEntity) {
        orderdetailsEntityFacade.edit(orderdetailsEntity);
    }

    public void remove(OrderdetailsEntity orderdetailsEntity) {
        orderdetailsEntityFacade.remove(orderdetailsEntity);
    }

    public OrderdetailsEntity find(Object id) {
        return orderdetailsEntityFacade.find(id);
    }

    public List<OrderdetailsEntity> findAll() {
        return orderdetailsEntityFacade.findAll();
    }

    public List<OrderdetailsEntity> findRange(int[] range) {
        return orderdetailsEntityFacade.findRange(range);
    }

    public int count() {
        return orderdetailsEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return orderdetailsEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return orderdetailsEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return orderdetailsEntityFacade.toString();
    }
    
}
