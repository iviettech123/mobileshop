/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.AccessoriesEntity;
import com.iviet.session.AccessoriesEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class AccessoriesEntityFacadeStub {
    AccessoriesEntityFacadeLocal accessoriesEntityFacade = lookupAccessoriesEntityFacadeLocal();

    private AccessoriesEntityFacadeLocal lookupAccessoriesEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (AccessoriesEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/AccessoriesEntityFacade!com.iviet.session.AccessoriesEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(AccessoriesEntity accessoriesEntity) {
        accessoriesEntityFacade.create(accessoriesEntity);
    }

    public void edit(AccessoriesEntity accessoriesEntity) {
        accessoriesEntityFacade.edit(accessoriesEntity);
    }

    public void remove(AccessoriesEntity accessoriesEntity) {
        accessoriesEntityFacade.remove(accessoriesEntity);
    }

    public AccessoriesEntity find(Object id) {
        return accessoriesEntityFacade.find(id);
    }

    public List<AccessoriesEntity> findAll() {
        return accessoriesEntityFacade.findAll();
    }

    public List<AccessoriesEntity> findRange(int[] range) {
        return accessoriesEntityFacade.findRange(range);
    }

    public int count() {
        return accessoriesEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return accessoriesEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return accessoriesEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return accessoriesEntityFacade.toString();
    }
    
}
