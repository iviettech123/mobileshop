/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.OrdersEntity;
import com.iviet.session.OrdersEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class OrdersEntityFacadeStub {
    OrdersEntityFacadeLocal ordersEntityFacade = lookupOrdersEntityFacadeLocal();

    private OrdersEntityFacadeLocal lookupOrdersEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (OrdersEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/OrdersEntityFacade!com.iviet.session.OrdersEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(OrdersEntity ordersEntity) {
        ordersEntityFacade.create(ordersEntity);
    }

    public void edit(OrdersEntity ordersEntity) {
        ordersEntityFacade.edit(ordersEntity);
    }

    public void remove(OrdersEntity ordersEntity) {
        ordersEntityFacade.remove(ordersEntity);
    }

    public OrdersEntity find(Object id) {
        return ordersEntityFacade.find(id);
    }

    public List<OrdersEntity> findAll() {
        return ordersEntityFacade.findAll();
    }

    public List<OrdersEntity> findRange(int[] range) {
        return ordersEntityFacade.findRange(range);
    }

    public int count() {
        return ordersEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return ordersEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return ordersEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return ordersEntityFacade.toString();
    }
    
}
