/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.StaffgroupEntity;
import com.iviet.session.StaffgroupEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class StaffgroupEntityFacadeStub {
    StaffgroupEntityFacadeLocal staffgroupEntityFacade = lookupStaffgroupEntityFacadeLocal();

    private StaffgroupEntityFacadeLocal lookupStaffgroupEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (StaffgroupEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/StaffgroupEntityFacade!com.iviet.session.StaffgroupEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(StaffgroupEntity staffgroupEntity) {
        staffgroupEntityFacade.create(staffgroupEntity);
    }

    public void edit(StaffgroupEntity staffgroupEntity) {
        staffgroupEntityFacade.edit(staffgroupEntity);
    }

    public void remove(StaffgroupEntity staffgroupEntity) {
        staffgroupEntityFacade.remove(staffgroupEntity);
    }

    public StaffgroupEntity find(Object id) {
        return staffgroupEntityFacade.find(id);
    }

    public List<StaffgroupEntity> findAll() {
        return staffgroupEntityFacade.findAll();
    }

    public List<StaffgroupEntity> findRange(int[] range) {
        return staffgroupEntityFacade.findRange(range);
    }

    public int count() {
        return staffgroupEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return staffgroupEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return staffgroupEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return staffgroupEntityFacade.toString();
    }
    
    
}
