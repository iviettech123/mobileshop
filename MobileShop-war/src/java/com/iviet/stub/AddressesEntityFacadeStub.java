/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.AddressesEntity;
import com.iviet.session.AddressesEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class AddressesEntityFacadeStub {
    AddressesEntityFacadeLocal addressesEntityFacade = lookupAddressesEntityFacadeLocal();

    private AddressesEntityFacadeLocal lookupAddressesEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (AddressesEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/AddressesEntityFacade!com.iviet.session.AddressesEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(AddressesEntity addressesEntity) {
        addressesEntityFacade.create(addressesEntity);
    }

    public void edit(AddressesEntity addressesEntity) {
        addressesEntityFacade.edit(addressesEntity);
    }

    public void remove(AddressesEntity addressesEntity) {
        addressesEntityFacade.remove(addressesEntity);
    }

    public AddressesEntity find(Object id) {
        return addressesEntityFacade.find(id);
    }

    public List<AddressesEntity> findAll() {
        return addressesEntityFacade.findAll();
    }

    public List<AddressesEntity> findRange(int[] range) {
        return addressesEntityFacade.findRange(range);
    }

    public int count() {
        return addressesEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return addressesEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return addressesEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return addressesEntityFacade.toString();
    }
    
}
