/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.ShipmentEntity;
import com.iviet.session.ShipmentEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class ShipmentEntityFacadeStub {
    ShipmentEntityFacadeLocal shipmentEntityFacade = lookupShipmentEntityFacadeLocal();

    private ShipmentEntityFacadeLocal lookupShipmentEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (ShipmentEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/ShipmentEntityFacade!com.iviet.session.ShipmentEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(ShipmentEntity shipmentEntity) {
        shipmentEntityFacade.create(shipmentEntity);
    }

    public void edit(ShipmentEntity shipmentEntity) {
        shipmentEntityFacade.edit(shipmentEntity);
    }

    public void remove(ShipmentEntity shipmentEntity) {
        shipmentEntityFacade.remove(shipmentEntity);
    }

    public ShipmentEntity find(Object id) {
        return shipmentEntityFacade.find(id);
    }

    public List<ShipmentEntity> findAll() {
        return shipmentEntityFacade.findAll();
    }

    public List<ShipmentEntity> findRange(int[] range) {
        return shipmentEntityFacade.findRange(range);
    }

    public int count() {
        return shipmentEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return shipmentEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return shipmentEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return shipmentEntityFacade.toString();
    }
    
}
