/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.NewsEntity;
import com.iviet.session.NewsEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class NewsEntityFacadeStub {
    NewsEntityFacadeLocal newsEntityFacade = lookupNewsEntityFacadeLocal();

    private NewsEntityFacadeLocal lookupNewsEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (NewsEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/NewsEntityFacade!com.iviet.session.NewsEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(NewsEntity newsEntity) {
        newsEntityFacade.create(newsEntity);
    }

    public void edit(NewsEntity newsEntity) {
        newsEntityFacade.edit(newsEntity);
    }

    public void remove(NewsEntity newsEntity) {
        newsEntityFacade.remove(newsEntity);
    }

    public NewsEntity find(Object id) {
        return newsEntityFacade.find(id);
    }

    public List<NewsEntity> findAll() {
        return newsEntityFacade.findAll();
    }

    public List<NewsEntity> findRange(int[] range) {
        return newsEntityFacade.findRange(range);
    }

    public int count() {
        return newsEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return newsEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return newsEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return newsEntityFacade.toString();
    }
    
}
