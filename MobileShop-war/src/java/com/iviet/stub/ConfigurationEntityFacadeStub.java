/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.ConfigurationEntity;
import com.iviet.session.ConfigurationEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class ConfigurationEntityFacadeStub {
    ConfigurationEntityFacadeLocal configurationEntityFacade = lookupConfigurationEntityFacadeLocal();

    private ConfigurationEntityFacadeLocal lookupConfigurationEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (ConfigurationEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/ConfigurationEntityFacade!com.iviet.session.ConfigurationEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(ConfigurationEntity configurationEntity) {
        configurationEntityFacade.create(configurationEntity);
    }

    public void edit(ConfigurationEntity configurationEntity) {
        configurationEntityFacade.edit(configurationEntity);
    }

    public void remove(ConfigurationEntity configurationEntity) {
        configurationEntityFacade.remove(configurationEntity);
    }

    public ConfigurationEntity find(Object id) {
        return configurationEntityFacade.find(id);
    }

    public List<ConfigurationEntity> findAll() {
        return configurationEntityFacade.findAll();
    }

    public List<ConfigurationEntity> findRange(int[] range) {
        return configurationEntityFacade.findRange(range);
    }

    public int count() {
        return configurationEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return configurationEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return configurationEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return configurationEntityFacade.toString();
    }
    
}
