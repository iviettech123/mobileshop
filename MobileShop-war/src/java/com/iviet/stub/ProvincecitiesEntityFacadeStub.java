/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.ProvincecitiesEntity;
import com.iviet.session.ProvincecitiesEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class ProvincecitiesEntityFacadeStub {
    ProvincecitiesEntityFacadeLocal provincecitiesEntityFacade = lookupProvincecitiesEntityFacadeLocal();

    private ProvincecitiesEntityFacadeLocal lookupProvincecitiesEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (ProvincecitiesEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/ProvincecitiesEntityFacade!com.iviet.session.ProvincecitiesEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(ProvincecitiesEntity provincecitiesEntity) {
        provincecitiesEntityFacade.create(provincecitiesEntity);
    }

    public void edit(ProvincecitiesEntity provincecitiesEntity) {
        provincecitiesEntityFacade.edit(provincecitiesEntity);
    }

    public void remove(ProvincecitiesEntity provincecitiesEntity) {
        provincecitiesEntityFacade.remove(provincecitiesEntity);
    }

    public ProvincecitiesEntity find(Object id) {
        return provincecitiesEntityFacade.find(id);
    }

    public List<ProvincecitiesEntity> findAll() {
        return provincecitiesEntityFacade.findAll();
    }

    public List<ProvincecitiesEntity> findRange(int[] range) {
        return provincecitiesEntityFacade.findRange(range);
    }

    public int count() {
        return provincecitiesEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return provincecitiesEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return provincecitiesEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return provincecitiesEntityFacade.toString();
    }

    public List<ProvincecitiesEntity> findAll2() {
        return provincecitiesEntityFacade.findAll2();
    }
    
    
   
    
}
