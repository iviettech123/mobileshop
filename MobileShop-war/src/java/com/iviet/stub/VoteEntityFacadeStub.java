/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.VoteEntity;
import com.iviet.session.VoteEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class VoteEntityFacadeStub {
    VoteEntityFacadeLocal voteEntityFacade = lookupVoteEntityFacadeLocal();

    private VoteEntityFacadeLocal lookupVoteEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (VoteEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/VoteEntityFacade!com.iviet.session.VoteEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(VoteEntity voteEntity) {
        voteEntityFacade.create(voteEntity);
    }

    public void edit(VoteEntity voteEntity) {
        voteEntityFacade.edit(voteEntity);
    }

    public void remove(VoteEntity voteEntity) {
        voteEntityFacade.remove(voteEntity);
    }

    public VoteEntity find(Object id) {
        return voteEntityFacade.find(id);
    }

    public List<VoteEntity> findAll() {
        return voteEntityFacade.findAll();
    }

    public List<VoteEntity> findRange(int[] range) {
        return voteEntityFacade.findRange(range);
    }

    public int count() {
        return voteEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return voteEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return voteEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return voteEntityFacade.toString();
    }
    
}
