/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.ImagesEntity;
import com.iviet.session.ImagesEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class ImagesEntityFacadeStub {
    ImagesEntityFacadeLocal imagesEntityFacade = lookupImagesEntityFacadeLocal();

    private ImagesEntityFacadeLocal lookupImagesEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (ImagesEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/ImagesEntityFacade!com.iviet.session.ImagesEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(ImagesEntity imagesEntity) {
        imagesEntityFacade.create(imagesEntity);
    }

    public void edit(ImagesEntity imagesEntity) {
        imagesEntityFacade.edit(imagesEntity);
    }

    public void remove(ImagesEntity imagesEntity) {
        imagesEntityFacade.remove(imagesEntity);
    }

    public ImagesEntity find(Object id) {
        return imagesEntityFacade.find(id);
    }

    public List<ImagesEntity> findAll() {
        return imagesEntityFacade.findAll();
    }

    public List<ImagesEntity> findRange(int[] range) {
        return imagesEntityFacade.findRange(range);
    }

    public int count() {
        return imagesEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return imagesEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return imagesEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return imagesEntityFacade.toString();
    }
    
}
