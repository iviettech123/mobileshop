/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.stub;

import com.iviet.entity.CustomersEntity;
import com.iviet.session.CustomersEntityFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author XuânBá
 */
public class CustomersEntityFacadeStub {
    CustomersEntityFacadeLocal customersEntityFacade = lookupCustomersEntityFacadeLocal();

    private CustomersEntityFacadeLocal lookupCustomersEntityFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (CustomersEntityFacadeLocal) c.lookup("java:global/MobileShop/MobileShop-ejb/CustomersEntityFacade!com.iviet.session.CustomersEntityFacadeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public void create(CustomersEntity customersEntity) {
        customersEntityFacade.create(customersEntity);
    }

    public void edit(CustomersEntity customersEntity) {
        customersEntityFacade.edit(customersEntity);
    }

    public void remove(CustomersEntity customersEntity) {
        customersEntityFacade.remove(customersEntity);
    }

    public CustomersEntity find(Object id) {
        return customersEntityFacade.find(id);
    }

    public List<CustomersEntity> findAll() {
        return customersEntityFacade.findAll();
    }

    public List<CustomersEntity> findRange(int[] range) {
        return customersEntityFacade.findRange(range);
    }

    public int count() {
        return customersEntityFacade.count();
    }

    @Override
    public int hashCode() {
        return customersEntityFacade.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return customersEntityFacade.equals(obj);
    }

    @Override
    public String toString() {
        return customersEntityFacade.toString();
    }
    
}
