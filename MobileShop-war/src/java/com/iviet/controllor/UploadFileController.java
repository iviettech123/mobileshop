/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iviet.controllor;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
/**
 *
 * @author XuânBá
 */
@ManagedBean
@SessionScoped
public class UploadFileController{
    
    private Part file1;
    private Part file2;

    public Part getFile1() {
        return file1;
    }

    public void setFile1(Part file1) {
        this.file1 = file1;
    }

    public Part getFile2() {
        return file2;
    }

    public void setFile2(Part file2) {
        this.file2 = file2;
    }
    
    
    
    public String upload() throws IOException{
        file1.write("faces\\site\\images\\products\\thumb\\" + getFilename(file1));
        file2.write("faces\\site\\images\\products\\thumb\\" + getFilename(file2));
        return "Success";
    }
    
    private static String getFilename(Part part){
        for(String cd: part.getHeader("Content-Disposition").split(";")){
            if(cd.trim().startsWith("filename")){
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') +1).substring(filename.lastIndexOf('\\') +1);
            }
        }
        return null;
    }
    
}